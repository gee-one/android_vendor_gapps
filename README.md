# Pico Style Gapps  

# Use at your own risk!!  

## Installation instructions
This is not fully tested yet and it may never be.  It works on my phone, YMMV.


Add this repo to your repo by adding this in .repo/local_manifests/gapps.xml

```
<?xml version="1.0" encoding="UTF-8"?>

<manifest>

  <remote  name="gee-one"
           fetch="https://gitlab.com/gee-one/android_vendor_gapps/"
           sync-j="6" />

  <project path="vendors/gapps" name="android_vendor_gapps" remote="gee-one" revision="android-13.0"/>

</manifest>
```


Add something like this to your device tree to build gapps in line.
```
$(call inherit-product-if-exists, vendor/gapps/common/common-vendor.mk)
```
